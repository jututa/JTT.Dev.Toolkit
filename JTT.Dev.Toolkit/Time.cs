﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTT.Dev.Toolkit
{
    /// <summary>
    /// 时间处理模块
    /// </summary>
    namespace Time
    {
        /// <summary>
        /// 时间处理辅助类
        /// </summary>
        public static class TimeHelper
        {

            /// <summary>
            /// 时间戳转换为时间
            /// </summary>
            /// <param name="_TimeStamp"></param>
            /// <returns></returns>
            public static DateTime ConvertToTime(this Int64 _TimeStamp)
            {
                DateTime TempDateTime = new DateTime();
                DateTime StartTime = new DateTime(1970, 1, 1);
                TempDateTime = Convert.ToDateTime(StartTime.AddSeconds(_TimeStamp));
                return TempDateTime;
            }

            /// <summary>
            /// 时间转换为时间戳数据
            /// </summary>
            /// <param name="_TimeStamp"></param>
            /// <returns></returns>
            public static Int64 ConvertToTimeStamp(this DateTime _DateTime)
            {
                DateTime StartTime = new DateTime(1970, 1, 1);
                TimeSpan TempTimeSpan = _DateTime.Subtract(StartTime).Duration();
                return Convert.ToInt64(TempTimeSpan.TotalSeconds);
            }
        }
    }
}
