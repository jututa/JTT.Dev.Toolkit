﻿using JTT.Dev.Toolkit.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JTT.Dev.Toolkit
{
    //SBS开发模式
    namespace SBS
    {
        /// <summary>
        /// SBS开发帮助辅助类
        /// </summary>
        public static class SBSHelper
        {
            public static String CombineSBSRESTString<T>(this T _Object, String _FuncName)
            {
                SBSRESTRequest MySBSRESTRequest = new SBSRESTRequest();
                MySBSRESTRequest.BusinessFunctionRouteName = _FuncName;
                MySBSRESTRequest.BusinessEntityString = _Object.ObjectToJson<T>();
                return MySBSRESTRequest.ObjectToJson<SBSRESTRequest>();
            }
        }

        /// <summary>
        /// REST请求实体
        /// </summary>
        [DataContract]
        public class SBSRESTRequest
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public SBSRESTRequest()
            {
            }

            /// <summary>
            /// 业务逻辑方法路由名称
            /// </summary>
            [DataMember(Name = "BusinessFunctionRouteName")]
            public String BusinessFunctionRouteName { get; set; }

            /// <summary>
            /// 业务逻辑实体封装字符串
            /// </summary>
            [DataMember(Name = "BusinessEntityString")]
            public String BusinessEntityString { get; set; }
        }

        /// <summary>
        /// REST响应实体
        /// </summary>
        [DataContract()]
        public class SBSRESTResponse
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public SBSRESTResponse()
            {
            }

            /// <summary>
            /// 业务逻辑方法路由名称
            /// </summary>
            [DataMember(Name = "BusinessFunctionRouteName")]
            public String BusinessFunctionRouteName { get; set; }

            /// <summary>
            /// 业务逻辑实体封装字符串
            /// </summary>
            [DataMember(Name = "BusinessEntityString")]
            public String BusinessEntityString { get; set; }
        }

        /// <summary>
        /// 基础服务器控制器
        /// </summary>
        public class SBSBaseServiceControl
        {
            /// <summary>
            /// 处理RESTService
            /// </summary>
            /// <param name="_Request"></param>
            /// <param name="_HttpRequest"></param>
            /// <returns></returns>
            public virtual String ProcessServiceMessage(String _BusinessEntityString, HttpRequestMessage _HttpRequest)
            {
                return "";
            }
        }

        /// <summary>
        /// 基础业务逻辑控制对象
        /// </summary>
        public class SBSBaseBusinessControl
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public SBSBaseBusinessControl() { }

            /// <summary>
            /// 处理业务逻辑
            /// </summary>
            /// <param name="_Request"></param>
            /// <param name="_HttpRequest"></param>
            /// <returns></returns>
            public virtual SBSRESTResponse ProcessBusiness(SBSRESTRequest _Request, HttpRequestMessage _HttpRequest)
            {
                return new SBSRESTResponse();
            }

            /// <summary>
            /// 带HTTP请求状态的处理业务逻辑
            /// </summary>
            /// <param name="_Request"></param>
            /// <param name="_HttpRequest"></param>
            /// <returns></returns>
            public virtual SBSRESTResponse ProcessBusiness(SBSRESTRequest _Request)
            {
                return new SBSRESTResponse();
            }

            /// <summary>
            /// 返回新的GUID
            /// </summary>
            /// <returns></returns>
            public String NewGuid()
            {
                return Guid.NewGuid().ToString("D").Replace("-", "").ToUpper();
            }

            /// <summary>
            ///  获取随机数，一般用于签名
            /// </summary>
            /// <param name="codeLen"></param>
            /// <returns></returns>
            public String CreateRandomCode(Int32 _CodeLength)
            {
                String TempCodeSerial = "2,3,4,5,6,7,a,c,d,e,f,h,i,j,k,m,n,p,r,s,t,A,C,D,E,F,G,H,J,K,M,N,P,Q,R,S,U,V,W,X,Y,Z";
                if (_CodeLength == 0)
                {
                    _CodeLength = 16;
                }
                String[] CodeArray = TempCodeSerial.Split(',');
                String TempCode = "";
                Int32 RandomValue = -1;
                Random rand = new Random(unchecked((Int32)DateTime.Now.Ticks));
                for (Int32 i = 0; i < _CodeLength; i++)
                {
                    RandomValue = rand.Next(0, CodeArray.Length - 1);
                    TempCode += CodeArray[RandomValue];
                }
                return TempCode;
            }
        }

        /// <summary>
        /// 基础业务请求类
        /// </summary>
        [DataContract()]
        public class SBSBaseBusinessRequest
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public SBSBaseBusinessRequest() { }
        }

        /// <summary>
        /// 基础业务响应类
        /// </summary>
        [DataContract()]
        public class SBSBaseBusinessResponse
        {
            /// <summary>
            /// 构造函数
            /// </summary>
            public SBSBaseBusinessResponse() { }

            /// <summary>
            /// 执行结果
            /// </summary>
            [DataMember(Name = "ExecuteResult")]
            public Int32 ExecuteResult { get; set; }

            /// <summary>
            /// 执行消息
            /// </summary>
            [DataMember(Name = "ExecuteMessage")]
            public String ExecuteMessage { get; set; }

            /// <summary>
            /// 设置执行结果
            /// </summary>
            /// <param name="_ExecuteResult"></param>
            /// <param name="_ExecuteMessage"></param>
            public void SetExecuteResult(Int32 _ExecuteResult, String _ExecuteMessage)
            {
                ExecuteResult = _ExecuteResult;
                ExecuteMessage = _ExecuteMessage;
            }
        }

        /// <summary>
        /// 带密钥的业务逻辑请求类
        /// </summary>
        [DataContract()]
        public class SBSTokenBusinessRequest : SBSBaseBusinessRequest
        {
            /// <summary>
            /// 请求密钥
            /// </summary>
            [DataMember(Name = "AccessToken")]
            public String AccessToken { get; set; }
        }

        /// <summary>
        /// 带密钥的业务逻辑响应类
        /// </summary>
        [DataContract()]
        public class SBSTokenBusinessResponse : SBSBaseBusinessResponse
        {
            /// <summary>
            /// 设置数据访问错误
            /// </summary>
            public void SetDataAccessError()
            {
                this.ExecuteResult = -5;
                this.ExecuteMessage = "数据库访问错误";
            }

            /// <summary>
            /// 设置权限访问错误
            /// </summary>
            public void SetAccessTokenError()
            {
                this.ExecuteResult = -5;
                this.ExecuteMessage = "数据库访问错误";
            }
        }

        /// <summary>
        /// 公开业务逻辑请求类
        /// </summary>
        [DataContract()]
        public class SBSPubBusinessRequest : SBSBaseBusinessRequest
        {
        }

        /// <summary>
        /// 公开业务逻辑响应类
        /// </summary>
        [DataContract()]
        public class SBSPubBusinessResponse : SBSBaseBusinessResponse
        {
            /// <summary>
            /// 设置数据访问错误
            /// </summary>
            public void SetDataAccessError()
            {
                this.ExecuteResult = -5;
                this.ExecuteMessage = "数据库访问错误";
            }
        }
    }
}
